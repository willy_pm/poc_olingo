package fr.deal.olingo.service;

import fr.deal.olingo.data.Storage;
import fr.deal.olingo.util.Util;
import org.apache.olingo.commons.api.data.ContextURL;
import org.apache.olingo.commons.api.data.ContextURL.Suffix;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.commons.api.format.ContentType;
import org.apache.olingo.commons.api.http.HttpHeader;
import org.apache.olingo.commons.api.http.HttpMethod;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.*;
import org.apache.olingo.server.api.deserializer.DeserializerException;
import org.apache.olingo.server.api.deserializer.DeserializerResult;
import org.apache.olingo.server.api.deserializer.ODataDeserializer;
import org.apache.olingo.server.api.processor.EntityProcessor;
import org.apache.olingo.server.api.serializer.EntitySerializerOptions;
import org.apache.olingo.server.api.serializer.ODataSerializer;
import org.apache.olingo.server.api.serializer.SerializerException;
import org.apache.olingo.server.api.serializer.SerializerResult;
import org.apache.olingo.server.api.uri.*;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class DemoEntityProcessor implements EntityProcessor {

    private OData odata;
    private ServiceMetadata serviceMetadata;
    private Storage storage;

    public DemoEntityProcessor(Storage storage) {
        this.storage = storage;
    }

    public void init(OData odata, ServiceMetadata serviceMetadata) {
        this.odata = odata;
        this.serviceMetadata = serviceMetadata;
    }

    public void readEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType responseFormat)
            throws ODataApplicationException, SerializerException {

        EdmEntityType responseEdmEntityType = null; // need this for build the context URL
        Entity responseEntity = null; // required for serialization of the response body
        EdmEntitySet responseEdmEntitySet = null; // need for building the context URL

        // 1 : retrieve the Entity
        // can be "normal" read operation or navigation (to-one)
        List<UriResource> resourceParts = uriInfo.getUriResourceParts();
        int segmentCount = resourceParts.size();

        UriResource uriResource = resourceParts.get(0);
        if (!(uriResource instanceof UriResourceEntitySet)) {
            throw new ODataApplicationException("Only EntitySet is supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.FRANCE);
        }
        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) uriResource;
        EdmEntitySet startEdmEntitySet = uriResourceEntitySet.getEntitySet();

        // Analyse the URI segments
        if (segmentCount == 1) { // no navigation
            responseEdmEntityType = startEdmEntitySet.getEntityType();
            responseEdmEntitySet = startEdmEntitySet;

            // 2. retrieve the data from backend
            List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
            responseEntity = storage.readEntityData(responseEdmEntitySet, keyPredicates);

        } else if (segmentCount == 2) { // navigation

            UriResource navSegment = resourceParts.get(1);
            if(!(navSegment instanceof UriResourceNavigation)) {
                throw new ODataApplicationException("Only RessourceNavigation is supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.FRANCE);
            }

            UriResourceNavigation uriResourceNavigation = (UriResourceNavigation) navSegment;
            EdmNavigationProperty edmNavigationProperty = uriResourceNavigation.getProperty();
            responseEdmEntityType = edmNavigationProperty.getType();
            responseEdmEntitySet = Util.getNavigationTargetEntitySet(startEdmEntitySet, edmNavigationProperty);

            // 2 : fetch the data from backend
            // for:  Products(1)/Category  we have to find the correct Category entity
            List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
            // e.g. for Products(1)/Category we have to find first the Products(1)
            Entity sourceEntity = storage.readEntityData(startEdmEntitySet, keyPredicates);

            // now we have to check if the navigation is
            // a) to-one: e.g. Products(1)/Category
            // b) to-many with key: e.g. Categories(3)/Products(5)
            List<UriParameter> navKeyPredicates = uriResourceNavigation.getKeyPredicates();

            if (navKeyPredicates.isEmpty()) {
                // e.g. DemoService.svc/Products(1)/Category
                responseEntity = storage.getRelatedEntity(sourceEntity, responseEdmEntityType);
            } else {
                // e.g. DemoService.svc/Categories(3)/Products(5)
                responseEntity = storage.getRelatedEntity(sourceEntity, responseEdmEntityType, navKeyPredicates);
            }
        } else {
            // this would be the case for e.g. Products(1)/Category/Products(1)/Category
            throw new ODataApplicationException("Not supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.FRANCE);
        }

        if(responseEntity == null) {
            // this is the case for e.g. DemoService.svc/Categories(4) or DemoService.svc/Categories(3)/Products(999)
            throw new ODataApplicationException("Nothing found.", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.FRANCE);
        }

        // 3. serialize
        ContextURL contextUrl = ContextURL.with().entitySet(responseEdmEntitySet).suffix(Suffix.ENTITY).build();
        EntitySerializerOptions opts = EntitySerializerOptions.with().contextURL(contextUrl).build();

        ODataSerializer serializer = this.odata.createSerializer(responseFormat);
        SerializerResult serializerResult = serializer.entity(this.serviceMetadata, responseEdmEntityType, responseEntity, opts);

        //4. configure the response object
        response.setContent(serializerResult.getContent());
        response.setStatusCode(HttpStatusCode.OK.getStatusCode());
        response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
    }




    /*
     * These processor methods are not handled in this tutorial
     * */


    public void createEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType requestFormat, ContentType responseFormat) throws ODataApplicationException, DeserializerException, SerializerException {

        // 1 : retrieve the entity type from the URI
        EdmEntitySet edmEntitySet = Util.getEdmEntitySet(uriInfo);
        EdmEntityType edmEntityType = edmEntitySet.getEntityType();

        // 2 : create the data in backend
        // 2.1 : retrieve the payload from the POST request for the entity to create and deserialize it
        InputStream requestInputStream = request.getBody();
        ODataDeserializer deserializer = this.odata.createDeserializer(requestFormat);
        DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);
        Entity requestEntity = result.getEntity();
        // 2.2 : do the creation in backend, which returns the newly created entity
        Entity createdEntity = storage.createEntityData(edmEntitySet, requestEntity);

        // 3 : serialize the response (we have to return the created entity)
        ContextURL contextURL = ContextURL.with().entitySet(edmEntitySet).build();
        EntitySerializerOptions options = EntitySerializerOptions.with().contextURL(contextURL).build();

        ODataSerializer serializer = this.odata.createSerializer(requestFormat);
        SerializerResult serializedResponse = serializer.entity(serviceMetadata, edmEntityType, createdEntity, options);

        // 4 : Configure the response object
        response.setContent(serializedResponse.getContent());
        response.setStatusCode(HttpStatusCode.CREATED.getStatusCode());
        response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());

    }


    public void updateEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType requestFormat, ContentType responseFormat) throws ODataApplicationException, DeserializerException, SerializerException {

        // 1 : Retrieve the entity set which belongs to the request entity
        List<UriResource> ressourcePaths = uriInfo.getUriResourceParts();
        // Note : only in our example we can assume that the first segment is the EntitySet
        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) ressourcePaths.get(0);
        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();
        EdmEntityType edmEntityType = edmEntitySet.getEntityType();

        // 2 : update the data in backend
        // 2.1 : retrieve the payload from the PUT request for the entity to be updated
        InputStream requestInputStream = request.getBody();
        ODataDeserializer deserializer = this.odata.createDeserializer(requestFormat);
        DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);
        Entity requestEntity = result.getEntity();
        // 2.2 : do the modification in backend
        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
        // Note that this updateEntity() method is invoked for both PUT and PATCH operations
        HttpMethod httpMethod = request.getMethod();
        storage.updateEntityData(edmEntitySet, keyPredicates, requestEntity, httpMethod);

        // 3 : configure the response object
        response.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());

    }


    public void deleteEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo) throws ODataApplicationException {

        // 1 : Retrieve the entity set which belongs to the requested entity
        List<UriResource> ressourcePaths = uriInfo.getUriResourceParts();
        // Note : only in ou example we can assume that the first segment is the entitySet
        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) ressourcePaths.get(0);
        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();

        // 2 : delete the data in backend
        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
        storage.deleteEntityData(edmEntitySet, keyPredicates);

        // 3 : Configure the response object
        response.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());

    }

}

