package fr.deal.olingo.service;

import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;
import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.apache.olingo.commons.api.edm.provider.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DemoEdmProvider extends CsdlAbstractEdmProvider {

    // Service Namespace
    public static final String NAMESPACE = "OData.Demo";

    // EDM Container
    public static final String CONTAINER_NAME = "Container";
    public static final FullQualifiedName CONTAINER = new FullQualifiedName(NAMESPACE, CONTAINER_NAME);

    // Entity Types Names
    public static final String ET_PRODUCT_NAME = "Product";
    public static final FullQualifiedName ET_PRODUCT_FQN = new FullQualifiedName(NAMESPACE, ET_PRODUCT_NAME);

    public static final String ET_CATEGORY_NAME = "Category";
    public static final FullQualifiedName ET_CATEGORY_FQN = new FullQualifiedName(NAMESPACE, ET_CATEGORY_NAME);

    // Entity Set Names
    public static final String ES_PRODUCTS_NAME = "Products";
    public static final String ES_CATEGORIES_NAME = "Categories";


    @Override
    public CsdlEntityType getEntityType(FullQualifiedName entityTypeName) {

        CsdlEntityType entityType = null;

        // this method is called for one of the EntityTypes that are configured in the Schema
        if(ET_PRODUCT_FQN.equals(entityTypeName)){

            //create EntityType properties
            CsdlProperty id = new CsdlProperty().setName("ID").setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());
            CsdlProperty name = new CsdlProperty().setName("Name").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
            CsdlProperty  description = new CsdlProperty().setName("Description").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

            // create PropertyRef for Key element
            CsdlPropertyRef propertyRef = new CsdlPropertyRef();
            propertyRef.setName("ID");

            // configure EntityType
            entityType = new CsdlEntityType();
            entityType.setName(ET_PRODUCT_NAME);
            entityType.setProperties(Arrays.asList(id, name, description));
            entityType.setKey(Collections.singletonList(propertyRef));

            // Implementation of the navigation between Product -> Category
            CsdlNavigationProperty navigationProperty = new CsdlNavigationProperty()
                    .setName("Category")
                    .setType(ET_CATEGORY_FQN)
                    .setNullable(false)
                    .setPartner("Products"); // to define a bi-directional relationship

            List<CsdlNavigationProperty> navigationPropertyList = new ArrayList<>();
            navigationPropertyList.add(navigationProperty);

            // Set all the navigation properties
            entityType.setNavigationProperties(navigationPropertyList);
        }
        else if (entityTypeName.equals(ET_CATEGORY_FQN)) {

            // Definition of the Category EntityType
            // Define properties
            CsdlProperty id = new CsdlProperty()
                    .setName("ID")
                    .setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());
            CsdlProperty name = new CsdlProperty()
                    .setName("Name")
                    .setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

            // Define PropertyRef for key element
            CsdlPropertyRef propertyRef = new CsdlPropertyRef().setName("ID");

            // Define navigation between Category -> Products (1->n)
            CsdlNavigationProperty navigationProperty = new CsdlNavigationProperty()
                    .setName("Products")
                    .setType(ET_PRODUCT_FQN)
                    .setCollection(true)
                    .setPartner("Category"); // Relation between Product -> Category (1->1)

            List<CsdlNavigationProperty> navigationPropertyList = new ArrayList<>();
            navigationPropertyList.add(navigationProperty);

            // Configure EntityType
            entityType = new CsdlEntityType()
                    .setName(ET_CATEGORY_NAME)
                    .setProperties(Arrays.asList(id, name))
                    .setKey(Collections.singletonList(propertyRef))
                    .setNavigationProperties(navigationPropertyList);

        }

        return entityType;

    }

    @Override
    public CsdlEntitySet getEntitySet(FullQualifiedName entityContainer, String entitySetName)  {

        CsdlEntitySet entitySet = null;

        if(entityContainer.equals(CONTAINER)){

            if(entitySetName.equals(ES_PRODUCTS_NAME)){
                entitySet = new CsdlEntitySet();
                entitySet.setName(ES_PRODUCTS_NAME);
                entitySet.setType(ET_PRODUCT_FQN);

                // define navigation
                CsdlNavigationPropertyBinding navigationPropertyBinding = new CsdlNavigationPropertyBinding()
                        .setPath("Category") // the name of the corresponding navigation property
                        .setTarget("Categories");// the target entity set, where the navigation property points to

                List<CsdlNavigationPropertyBinding> navigationPropertyBindings = new ArrayList<>();
                navigationPropertyBindings.add(navigationPropertyBinding);

                // Set the list of navigation properties
                entitySet.setNavigationPropertyBindings(navigationPropertyBindings);

                return entitySet;
            }
            else if (entitySetName.equals(ES_CATEGORIES_NAME)) {

                entitySet = new CsdlEntitySet()
                        .setName(ES_CATEGORIES_NAME)
                        .setType(ET_CATEGORY_FQN);

                CsdlNavigationPropertyBinding navigationPropertyBinding = new CsdlNavigationPropertyBinding()
                        .setPath("Products") // the name of the corresponding navigation property
                        .setTarget("Products"); // the target entity set, where the navigation property points to

                List<CsdlNavigationPropertyBinding> navigationPropertyBindingList = new ArrayList<>();
                navigationPropertyBindingList.add(navigationPropertyBinding);

                entitySet.setNavigationPropertyBindings(navigationPropertyBindingList);
            }
        }

        return entitySet;

    }

    @Override
    public CsdlEntityContainerInfo getEntityContainerInfo(FullQualifiedName entityContainerName) {
        // This method is invoked when displaying the service document
        // at e.g. http://localhost:8080/DemoService/DemoService.svc
        if(entityContainerName == null || entityContainerName.equals(CONTAINER)){
            CsdlEntityContainerInfo entityContainerInfo = new CsdlEntityContainerInfo();
            entityContainerInfo.setContainerName(CONTAINER);
            return entityContainerInfo;
        }

        return null;

    }

    @Override
    public List<CsdlSchema> getSchemas() {
        // create Schema
        CsdlSchema schema = new CsdlSchema();
        schema.setNamespace(NAMESPACE);

        // add EntityTypes
        List<CsdlEntityType> entityTypes = new ArrayList<CsdlEntityType>();
        entityTypes.add(getEntityType(ET_PRODUCT_FQN));
        entityTypes.add(getEntityType(ET_CATEGORY_FQN));
        schema.setEntityTypes(entityTypes);

        // add EntityContainer
        schema.setEntityContainer(getEntityContainer());

        // finally
        List<CsdlSchema> schemas = new ArrayList<CsdlSchema>();
        schemas.add(schema);

        return schemas;

    }

    @Override
    public CsdlEntityContainer getEntityContainer() {
        // create EntitySets
        List<CsdlEntitySet> entitySets = new ArrayList<CsdlEntitySet>();
        entitySets.add(getEntitySet(CONTAINER, ES_PRODUCTS_NAME));
        entitySets.add(getEntitySet(CONTAINER, ES_CATEGORIES_NAME));

        // create EntityContainer
        CsdlEntityContainer entityContainer = new CsdlEntityContainer();
        entityContainer.setName(CONTAINER_NAME);
        entityContainer.setEntitySets(entitySets);

        return entityContainer;

    }



}
